export const isDev = process.env.NODE_ENV !== 'production';

export const OOPS_MESSAGE = 'Sorry! Something went wrong, please try refreshing the page or going back to a previous page and trying again.';

export const API_KEY = process.env.API_KEY;

export const dishwashersUrl = 'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher';
// export const dishwashersUrl = isDev
//    ? 'http://localhost:3002/api/products'
//    : 'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI';

export const dishwasherUrl = 'https://api.johnlewis.com/mobile-apps/api/v1/products/';



