# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. 

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3002](http://localhost:3002) with your browser.

---

# Dishwasher App Development Notes


## Intro
Thank you for allowing me the opportunity to complete this technical test.  It has taken much longer than expected mostly as my React Testing Library (RTL) knowledge was very basic.  Learning RTL and implementing a more TDD approach at the same time was time consuming but I have gained some good experience in this area doing so.


## Assumptions

  - Assumed price value from real API data is this value: `£${product.variantPriceRange.value.max}`;  The price found in mock data  value assumed as: `£${product.price.now}`;  App currently supports both.  Correct format needs confirmation. 

  - Cache settings for getServerSideProds are a guess and could be better set my monitoring page usage in production.

## Notes

### Testing Strategy Notes

Although I have worked on many .Net projects implementing automated unit tests, this is my first project taking a more TDD approach ("red light / green light") and working with Jest/React Testing Library.

I have taken a TDD approach with testing the Product-List and Product-Detail Pages from a user perspective of looking/interacting with them.  I have then supplemented that with some light unit testing of some of the components, typically to touch on support for "unhappy paths" of usage, to ensure the components at least complete without throwing errors when they are used with badly formed/missing props.  Hopefully giving each component a higher level of robustness.
### Setup / Technical Notes

- Upgrade node-sass from 5.0.0 to 7.0.1 and moved to devDependancies to remove vulnerabilities  -> sass-dart -> sass
- React 17.0.1 to 17.0.2 to support Next.js v12
- Next.js v12 to remove vulnerabilities
- Upgraded React from 17.0.1 to v17.0.2 to remove vulnerabilities - Then updated React v18 to allow use of React Testing Library - I was struggling with configuring RTL with v27.0.2 and had to move on.  React v18 has improved performance - but maybe a little young to use.
- Struggled to setup React Testing Library (as per Next.js guide) - required React v18 - tried earlier guides TNA.
- jest-environment-jsdom required for jest v28.  https://stackoverflow.com/questions/72013449/upgrading-jest-to-v28-error-test-environment-jest-environment-jsdom-cannot-be (also re-installing jest packages helped).

- I could remove these components as not used:
    "lodash": "^4.17.21",
    "node-fetch": "^3.2.5",

### Design Notes

- Product Page Responsive layout is driven through container divs wrapping the components to separate layout logic from components - gives them more reusability on other pages in the future.
 
- Many of the product components accept product as a prop rather than cluttering component calls with many props (they will only increase as component complexity does).  This is coupling the components closer to the data but if not there then the coupling would be in the page anyway.  I was on the fence with this one!

- CSS design - I have kept the globals.scss and left it untouched (a bit like the real jl-styles).  Then built any component styles on top of that.  

### Performance Notes
- GetServerSideProps - Data returned is too large[*] causing page load to be slow on slower connections - This gives the user an unhelpful blank page whilst data is loaded and page is hydrated then returned to client.  Maybe load data client side with uesEffect() so initial loading page can be rendered - I guess at the cost SEO though.  Further research/collaboration needed for a faster solution without impact on SEO.  See my suggestions below.  [Note: getServerSideProps is quite new to me]. 

  - Implement GetServerSideProps caching so slower page creation is performed much less often and in background when cache is stale - done 

  - Return lighter "stripped down" product data object from GetServerSideProps with same structure but supporting "used" properties only. 

  [*] "Warning: data for page "/" is 210 kB, this amount of data can reduce performance. See more info here: https://nextjs.org/docs/messages/large-page-data"


### Security Notes

Security - product-detail better checks against damage / code injection into the url via the id - Currently there is a basic "Id is numeric check".  No recommended patterns easily found on google for this.  Numeric check should be safe.  I would normally take advise from others or check our existing code for patterns already used.

Security - getAllDishwashers - improve the query string building.... use new URL() instead.  URL() is not well supported by Safari so holding off on this.  All data for the built URL are internal so will be safe.  I would normally take advise from others or check our existing code for patterns already used.

Security - Check how safe dangerouslySetInnerHTML is in the context we are using it.  Alternatives?  No alternatives, all data is internal so using this should be safe.  Could use DomPurify as an additional precaution before rendering but that does not work for server side rendering unless using Isomorphic-DOMPurify.


## Instructions

 - Please note teh dev port has been changed to 3002.

 - utils/logger.jsx logToConsole = true to turn logging on.

 - Errors from getServerSideProps are not thrown, they will be console.logged only regardless of logToConsole setting.  Throwing error causes next.js to present a 500 page.  I want to return to page and allow page to render a more friendly message.  Normally these errors would be sent to a monitoring tool so they are visible, that can be hooked into the logger.js file.

## Improvement Suggestions - Design

- Image Carousel needs bigger buttons to perform Next/Previous Image than the small dots below the images - I have implemented non-intrusive Next/Previous buttons on the image.  Maybe they should be a little more visible.

- For landscape layout I would suggest that the Product Information should sit under or above the price section on the right hand side to make more use of the full screen.

- Many of the JLP images don't have a white background, it would be nice if the backgrounds were more consistent - Just being fussy.

## Improvement Suggestions - Development

- Too much data returned from getServerSideProps to page - See Performance notes for suggestions.  (getServerSideProps is new to me).

- Tests a little light in some areas, especially interactive tests, e.g. Carousel buttons.

- Possibly limit content width to 1200px for desktops, but allow headings to cover full width of viewport.  This will reduce the image size and allow for more of the image to show without having to scroll.

- See "Backlog High" items listed below

- See "Backlog" items below - if you are still awake :-)

## Backlog - High

 - todo: Test - Test index and product detail pages by mocking (jest mocks) fetch (will test service layer).  Mock success, error responses and timeouts - DONE
 
 - todo: Test the "OOPS Sorry!" message for index and product details pages when data load fails in getServerSideProps.  Tested manually for now - Mocked fetch so now DONE

 - todo: A11y - Add html lang attribute for screen readers - (via _document.jsx) DONE

 - todo: Quality - App still using default vercel favicon - DONE

 - todo: Performance - image srcset - Add to product detail page - DONE
 - todo: Performance - image srcset - improve smaller screen size support - DONE
 - todo: Performance - image loading= eager (default) vs lazy loading.  Carousel best as eager - DONE
 - todo: Performance - make 1st image of carousel a priority load - DONE

 - todo: Assumption - assumed price value from real API data is this value: `£${product.variantPriceRange.value.max}`;  The price found in mock data  value assumed as: `£${product.price.now}`;  App currently supports both.  Correct format needs confirmation. 

 - todo: Config - getServerSideProps cache settings should not be hard coded

 - todo: Improve data fetching - fetch timeout support -> switch to axios or SWR for retries etc? or... Use node-fetch rather than fetch? Configure shorter timeouts.

## Backlog

 - todo: Test - Test Carousel button clicks on Next, Prev and dot buttons.

 - todo: Test - Test the other More arrow button click

 - todo: Test - Add missing Unit tests for some components "unhappy paths" e.g. missing or badly formed/typed prop objects - Some components done
    - todo: Product List / Product ListItem "unhappy path" tests could be improved

 - todo: Test - product-feature-list - unit test feature list rendered ok test missing - low as already tested in product-detail page test 

 - todo: Refactor - Carousel is a little "jumpy on page" when switching between different image sizes -> Sizing of real API images appears more consistent than images referenced in mock date - maybe just luck though.  Improve css to handle these image size differences better.  srcSet is correcting the image sizes.

 - todo: Quality - Safer arrow buttons for back (<), read more (>), read less (<)... font support, use svg instead.  Using lt gt that may render differently depending on supported fonts

 - todo: Ally - aria-label missing for "Read More" **arrow** button - needs to switch descriptions as pressed/}

 - todo: Quality - product list item heading text should be darker colour - unable todo with fonts supported levels of bolds - need to ensure price still "pops" and is not lost in product heading text. 

 - todo: Quality - ProductListItem validate product prop for correctness

 - todo: Quality - ProductTitleBar - better no props or malformed props validation... setup tests first

 - todo: Quality - product carousel - this margin is centering image - change approach to something more reliable -> "margin: auto"

 - todo: Quality - product-list-item - add tests for unhappy paths, i.e. no props, malformed props

 - todo: Quality - Product FeatureList - validate/test/default props

 - todo: Refactor - h1's duplicated from index page heading - refactor and reuse... Sass

 - todo: Refactor - reduce some of the CSS repetition with SASS features. 

 - testing jlp kit setup
 




