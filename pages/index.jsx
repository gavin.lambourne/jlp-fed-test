import Head from 'next/head';
import styles from './index.module.scss';
import ProductListHeading from '../components/product-list/product-list-heading/product-list-heading';
import ProductList from '../components/product-list/product-list/product-list';
import DisplayFailure from '../components/framework/display-failure/display-failure';

import { getAllDishwashers } from '../services/productsService';
import { logger, logError } from '../utils/logging';
import { OOPS_MESSAGE } from '../config';

export async function getServerSideProps({ req, res }) {
  // Note: Fresh for 60secs, stale for 120 secs (stale pages provided for 120secs whilst new page build in background)
  res.setHeader(
    'Cache-Control',
    'public, s-maxage=60, stale-while-revalidate=120'
  )

  logger(`index: getServerSideProps: starting fetch`);

  let data = null;
  let ok = true;
  try {
    //throw new Error('Test Error handling');
    data = await getAllDishwashers(1, 20);
  } catch (err) {
    ok = false;
    logError(err, true);    // silence=true : log error on server and don't response with 500 to page, let page handle with a friendly message
  }

  logger(`index: getServerSideProps: finished fetch`);
  return {
    props: {
      data: data,
      ok: ok,

    },
  };
}

const Home = ({ data, ok }) => {

  const products = data && Array.isArray(data?.products) ? data.products : null;

  return (
    <>
      <Head>
        <title>Dishwashers | JL &amp; Partners</title>
        <meta name="keywords" content="dishwashers, shopping" />
      </Head>
      <div className={styles.content} >
        <ProductListHeading heading="Dishwashers" count={products?.length} />
        {!ok && <DisplayFailure failureMessage={OOPS_MESSAGE} />}
        {ok && <ProductList products={products} />}
      </div>
    </>
  );
};

export default Home;
