import Head from 'next/head';
import ProductTitleBar from '../../components/product/product-title-bar/product-title-bar';
import ProductCarousel from '../../components/product/product-carousel/product-carousel';
import ProductFeatureList from '../../components/product/product-feature-list/product-feature-list';
import ProductPricePanel from '../../components/product/product-price-panel/product-price-panel';
import ProductInfoPanel from '../../components/product/product-info-panel/product-info-panel';
import DisplayFailure from '../../components/framework/display-failure/display-failure';

import { getDishwasher } from '../../services/productsService';
import { validateUrlId } from '../../utils/validation';
import { OOPS_MESSAGE } from '../../config';
import { logger, logError } from '../../utils/logging';

import styles from './product-detail.module.scss';

export async function getServerSideProps({ params, res }) {
  logger(`product-detail: getServerSideProps: started`);

  // Note: Fresh for 60secs, stale for 120 secs (stale pages provided for 120secs whilst new page build in background)
  res.setHeader(
    'Cache-Control',
    'public, s-maxage=60, stale-while-revalidate=120'
  );

  const id = params.id;
  logger(`product-detail:  getServerSideProps: id=${id}`);

  let product = null;
  let ok = true;
  try {
    if (!validateUrlId(id)) throw new Error(`Url ID is not valid`);
    //throw new Error('Test Error handling');

    product = await getDishwasher(id);
  } catch (err) {
    ok = false;
    logError(err, true); // log it on server and don't response with 500 to page, let page handle with a friendly message
  }

  logger(`product-detail: getServerSideProps: finished fetch`);
  return {
    props: {
      product: product,
      ok: ok,
    },
  };
}

const ProductDetail = ({ product, ok }) => {

  const title = `${product?.title} | JL &amp; Partners`;
  
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="keywords" content="dishwashers, shopping" />
      </Head>
      <div className={styles.content}>
        <div className={styles.headingContainer}>
          <ProductTitleBar product={product} />
        </div>

        {!ok && <DisplayFailure failureMessage={OOPS_MESSAGE} />}

        {ok && (
          <>
            <div className={styles.imageContainer}>
              <ProductCarousel product={product} />
            </div>
            <div className={styles.priceContainer}>
              <div className={styles.priceSeparator} />
              <ProductPricePanel product={product} />
            </div>

            <div className={styles.infoContainer}>
              <ProductInfoPanel product={product} />
            </div>

            <div className={styles.featuresContainer}>
              <ProductFeatureList product={product} />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default ProductDetail;
