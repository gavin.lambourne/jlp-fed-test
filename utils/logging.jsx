import { isDev } from '../config';

export const logToConsole = false;

// logger
export const logger = (message, level = 1) => {
  const currentLevel = 1;

  if ( isDev && logToConsole && level <= currentLevel) {
    console.log(message);
  }
};

// error logger
export const logError = (error, silence) => {
  // ready to log to analytics tool like Sentry, Bugsnag or Datadog.
  // for now let it throw to console
  if (!silence) throw error;
  logger(error);
};
