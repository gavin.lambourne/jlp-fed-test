// config taken from tech-finder (JLP Standards)
module.exports = {
    singleQuote: true,
    jsxBracketSameLine: false,
    printWidth: 80,
  };
  