import { render, screen } from '@testing-library/react';
import ProductListItem from './product-list-item';
import data from '../../../mockData/data.json';

const product = data.products[0];

describe('Product List Item Tests', () => {
  it('Renders one image tag with correct alt and url from product', () => {
    render(<ProductListItem product={product} />);

    const images = screen.getAllByRole('img');
    expect(images).toHaveLength(1);
    expect(images[0]).toHaveAttribute('alt', product.title);
    expect(images[0]).toHaveAttribute('src', product.image);
    //screen.debug();
  });

  it('Renders one correct price from product', () => {
    render(<ProductListItem product={product} />);

    const prices = screen.getAllByTestId('product-item-price');
    expect(prices).toHaveLength(1);
    expect(prices[0]).toHaveTextContent(product.price.now);
  });

  it('Renders one correct product title as a heading', () => {
    render(<ProductListItem product={product} />);

    const headings = screen.getAllByRole('heading');
    expect(headings).toHaveLength(1);
    expect(headings[0]).toHaveTextContent(product.title);
  });

  it('Renders link to product details page', () => {
    render(<ProductListItem product={product} />);

    const links = screen.getAllByRole('link');

    expect(links).toHaveLength(1);
    expect(links[0]).toHaveTextContent(product.title);

    expect(links[0]).toHaveAttribute(
      'href',
      `/product-detail/${product.productId}`
    );
  });
});

