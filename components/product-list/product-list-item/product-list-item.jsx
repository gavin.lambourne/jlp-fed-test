import styles from './product-list-item.module.scss';
import Link from 'next/link';

const ProductListItem = ({ product }) => {
  let priceString = '';
  if (product?.variantPriceRange?.value?.max) {
    priceString = `£${product.variantPriceRange.value.max}`; // assumption - assumed price value from real API data
  } else if (product?.price?.now) {
    priceString = `£${product.price.now}`; // assumption - price found in mocke data - TBC 2 JSON formats need to be supported?, or is this old mock data?
  }

  const image = product?.image;
  const cmds = '$cms-max-image-threshold$';

  const srcset = `${image}${cmds}&wid=288&fit=hfit,1 288w,
  ${image}${cmds}&wid=576&fit=hfit,1 576w,
  ${image}${cmds}&wid=672&fit=hfit,1 672w,
  ${image}${cmds}&wid=768&fit=hfit,1 768w,
  ${image}${cmds}&wid=896&fit=hfit,1 896w,
  ${image}${cmds}&wid=1024&fit=hfit,1 1024w,
  ${image}${cmds}&wid=1112&fit=hfit,1 1112w,
  ${image}${cmds}&wid=1200&fit=hfit,1 1200w,
  ${image}${cmds}&wid=1368&fit=hfit,1 1368w,
  ${image}${cmds}&wid=1536&fit=hfit,1 1536w,
  ${image}${cmds}&wid=1792&fit=hfit,1 1792w,
  ${image}${cmds}&wid=2048&fit=hfit,1 2048w`;

  const sizes = `(min-width: 2048px) 300px, 
    (min-width: 1536px) 300px, 
    (min-width: 1200px) 300px, 
    (min-width: 1024px) 300px, 
    (min-width: 768px) 256px, 
    (min-width: 576px) 100vw, 
    100vw`;

  return (
    <div className={styles.content}>
      <Link
        className={styles.link}
        href={{
          pathname: '/product-detail/[id]',
          query: { id: product?.productId },
        }}
      >
        <a className={styles.link} data-testid="product-item-link">
          <div className={styles.content}>
            <img
              src={image}
              srcSet={srcset}
              sizes={sizes}
              alt={product?.title}
              className={styles.img}
              loading="lazy"
            />

            <h2 className={styles.title} data-testid="product-item-title">
              {product?.title}
            </h2>
            <div
              className={styles.price}
              aria-label="price"
              data-testid="product-item-price"
            >
              {priceString}
            </div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default ProductListItem;
