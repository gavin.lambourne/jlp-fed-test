import ProductListItem from '../product-list-item/product-list-item';
import styles from './product-list.module.scss';

const ProductList = ({ products: products = [] }) => {
  return (
    <div className={styles.content}>
      {!products?.length > 0 ? (
        <div>No items found</div>
      ) : (
        products.map((product) => (
          <ProductListItem
            key={product.productId}
            product={product}
          />
        ))
      )}

    </div>
  );
};

export default ProductList;
