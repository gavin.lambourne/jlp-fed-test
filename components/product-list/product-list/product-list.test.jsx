import { render, screen } from '@testing-library/react';
import ProductList from './product-list';
import mockData from '../../../mockData/data.json';

const productCount = 5;

describe('Product List Tests', () => {
  // Sanity check
  it('Renders expected number of items in list', () => {
    const data = mockData.products.slice(0, productCount);
    render(<ProductList products={data} />);

    const elements = screen.getAllByTestId('product-item-title');
    expect(elements.length).toBe(productCount);
  });

  //unit test "unhappy" paths
  it('Renders "No items found" message when incorrect object structure passed, i.e. does not crash!', () => {
    const noData = {};
    render(<ProductList product={noData} />);

    const div = screen.queryByText(/No items found/i);
    expect(div).toBeInTheDocument();
  });

  it('Renders "No items found" when no props passed, i.e. does not crash!', () => {
    render(<ProductList />);

    const div = screen.queryByText(/No items found/i);
    expect(div).toBeInTheDocument();
  });
});
