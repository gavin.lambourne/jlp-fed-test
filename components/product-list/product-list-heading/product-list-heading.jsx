import styles from './product-list-heading.module.scss';

const ProductListHeading = ({ heading = 'Found', count = 0 }) => {
  const headingText = `${heading} (${count})`;
  return (
    <div>
      <div className={styles.content}>
        <h1>{headingText}</h1>
      </div>
    </div>
  );
};

export default ProductListHeading;
