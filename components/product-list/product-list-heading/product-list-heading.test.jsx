import { render, screen } from '@testing-library/react';
import ProductListHeading from './product-list-heading';

describe('Product List Heading Tests', () => {
  it('Renders heading title and count in brackets from supplied props', () => {
    const testCount = 1;
    render(<ProductListHeading heading="Dishwashers" count={testCount} />);

    const headingElement = screen.getByText(/Dishwashers/i);
    expect(headingElement).toBeInTheDocument();
    expect(headingElement).toHaveTextContent(`(${testCount})`);
  });

  it('Renders zero count when no count prop supplied', () => {
    render(<ProductListHeading heading="Ovens" />);

    const headingElement = screen.getByText(/Ovens/i);
    expect(headingElement).toBeInTheDocument();
    expect(headingElement).toHaveTextContent(`(0)`);
  });

  it('Renders default "Found" heading when no heading supplied', () => {
    const testCount = 1;
    render(<ProductListHeading count={testCount} />);
    const headingElement = screen.getByText(/Found/i);
    expect(headingElement).toBeInTheDocument();
    expect(headingElement).toHaveTextContent(`(${testCount})`);
  });

  it('Renders default "Found (0)" heading when no props supplied', () => {
    const testCount = 0;
    render(<ProductListHeading />);
    const headingElement = screen.getByText(/Found/i);
    expect(headingElement).toBeInTheDocument();
    expect(headingElement).toHaveTextContent(`(${testCount})`);
  });
});

