import { render, screen } from '@testing-library/react';
import ProductCarousel from './product-carousel';
import mockData from '../../../mockData/data2.json';

const product = mockData.detailsData[0];

describe('Product Carousel Tests', () => {

  // sanity check
  it('Renders a product image carousel', () => {
    render(<ProductCarousel product={product} />);

    const expectedCount = product.media.images.urls.length;
    const images = screen.queryAllByRole('img', {name : product.title});
    expect(images).toHaveLength(expectedCount);
}  );
  
  //unit test "unhappy" paths
  it('Renders "No Product Images" text when incorrect object structure passed, i.e. does not crash!', () => {
    const noData = {};
    render(<ProductCarousel product={noData} />);
    
    const div = screen.queryByText('No Product Images');
    expect(div).toBeInTheDocument();
    //screen.debug();
  });
  
  it('Renders "No Product Images" text when no props passed, i.e. does not crash!', () => {
    render(<ProductCarousel />);
    
    const div = screen.queryByText('No Product Images');
    expect(div).toBeInTheDocument();
    //screen.debug();
  });
  
});