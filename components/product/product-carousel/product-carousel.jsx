import styles from './product-carousel.module.scss';
import { logger } from '../../../utils/logging';
import { useState } from 'react';

const ProductCarousel = ({ product = {}, defaultImagePos = 0 }) => {
  const images = product?.media?.images;
  const [currentPos, setCurrentPos] = useState(defaultImagePos);
  const [imagesClasses, setImagesClasses] = useState([]);

  const onPrevClick = (e) => {
    handleSlide(-1);
  };
  const onNextClick = (e) => {
    handleSlide(1);
  };

  const onDotClick = (e) => {
    const stringPos = e.target.id.indexOf('Dot-');
    const indexString = e.target.id.substring(stringPos + 4);
    handleJump(~~indexString);
  };

  const handleSlide = (direction) => {
    logger(`ProductCarousel: handleSlide: direction=${direction}`);

    let newIC = [...imagesClasses];
    newIC[currentPos] = buildInactiveClasses();
    logger(`ProductCarousel: handleSlide: oldPos=${currentPos} `);

    let newPos = 0;
    if (direction === 1 && currentPos === images.urls.length - 1) {
      newPos = 0;
    } else if (direction === -1 && currentPos === 0) {
      newPos = images.urls.length - 1;
    } else {
      newPos = currentPos + direction;
    }

    logger(`ProductCarousel: handleSlide: newPos=${newPos} `);
    newIC[newPos] = buildActiveClasses();
    setImagesClasses(newIC);
    setCurrentPos(newPos);
  };

  const handleJump = (pos) => {
    logger(`ProductCarousel: handleJump: pos=${pos}`);

    let newIC = [...imagesClasses];
    newIC[currentPos] = buildInactiveClasses();
    logger(`ProductCarousel: handleJump: oldPos=${currentPos} `);

    let newPos = 0;
    if (newPos > images.urls.length - 1) {
      newPos = 0;
    } else if (newPos < 0) {
      newPos = images.urls.length - 1;
    } else {
      newPos = pos;
    }

    newIC[newPos] = buildActiveClasses();
    setImagesClasses(newIC);
    setCurrentPos(newPos);
  };

  const baseClasses = `${styles.imageContainer} ${styles.fade}`;
  const buildActiveClasses = () => {
    return {
      imageClass: `${baseClasses} ${styles.visible}`,
      dotClass: `${styles.dot} ${styles.active2}`,
    };
  };

  const buildInactiveClasses = () => {
    return {
      imageClass: `${baseClasses} ${styles.invisible}`,
      dotClass: `${styles.dot}`,
    };
  };

  for (let i = 0; i < images?.urls?.length; i++) {
    imagesClasses.push(buildInactiveClasses());
    logger(`ProductCarousel: i=${i} class=${imagesClasses[i].imageClass} `);
  }
  imagesClasses[currentPos] = buildActiveClasses();

  // image optimisation
  const generateSrcset = (
    image,
    cmds
  ) => `${image}${cmds}$rsp-pdp-port-320$ 320w, 
   ${image}${cmds}$rsp-pdp-port-640$ 640w`;
  const generateSrc = (image, cmds) => `${image}${cmds}$rsp-pdp-port-640$`;

  const sizes = '(max-width: 767px) 100vw, (min-width: 768px) 50vw';

  return (
    <div className={styles.content}>
      {images?.urls?.length > 0 ? (
        <>
          {images?.urls?.map((imageUrl, imageIndex) => (
            <div
              key={imageIndex}
              className={imagesClasses[imageIndex].imageClass}
            >
              <img
                src={generateSrc(imageUrl, '')}
                alt={product?.title}
                className={styles.image}
                loading="eager"
                srcSet={generateSrcset(imageUrl, '')}
                sizes={sizes}
                fetchpriority={imageIndex === 0 ? 'high' : 'auto'}
              />
            </div>
          ))}

          <button
            className={styles.prev}
            onClick={onPrevClick}
            aria-label="Previous Product Image"
          >
            &#10094;
          </button>
          <button
            className={styles.next}
            onClick={onNextClick}
            aria-label="Next Product Image"
          >
            &#10095;
          </button>

          <div className={styles.dotsContainer}>
            {images?.urls?.map((image, imageIndex) => (
              <button
                id={`carouselDot-${imageIndex}`}
                key={imageIndex}
                className={imagesClasses[imageIndex].dotClass}
                onClick={onDotClick}
                aria-label={`Select Product Image ${imageIndex}`}
              ></button>
            ))}
          </div>
        </>
      ) : (
        <div className={styles.noImages}>No Product Images</div>
      )}
    </div>
  );
};

export default ProductCarousel;
