import { render, screen } from '@testing-library/react';
import ProductInfoPanel from './product-info-panel';
import mockData from '../../../mockData/data2.json';

const product = mockData.detailsData[0];

describe('Product Info Panel Tests', () => {
  it('Renders product info from product (warning: only checks first 40 chars exist)', () => {
    render(<ProductInfoPanel product={product} />);

    const div = screen.queryByText(new RegExp('Boasting plenty of convenient features', 'i'));
    expect(div).toBeInTheDocument();
  });
  
  // unit test "unhappy" paths
  it('Renders blank details text when incorrect object structure passed, i.e. does not crash!', () => {
    const noData = {};
    render(<ProductInfoPanel product={noData} />);
    
    const div = screen.getByTestId('product-info-text');
    expect(div).toBeEmptyDOMElement();
    //screen.debug();
  });
  
  it('Renders blank details text when no props passed, i.e. does not crash!', () => {
    render(<ProductInfoPanel />);
    
    const div = screen.getByTestId('product-info-text');
    expect(div).toBeEmptyDOMElement();
    //screen.debug();
  });
  
});