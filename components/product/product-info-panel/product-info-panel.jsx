import styles from './product-info-panel.module.scss';
import { useState } from 'react';

const ProductInfoPanel = ({ product }) => {
  const onMoreClick = () => {
    setShowFullInfo((i) => !i);
  };

  let info = '';

  const [showFull, setShowFullInfo] = useState(false);

  if (product?.details?.productInformation) {
    if (!showFull) {
      info = `${product?.details?.productInformation
        ?.substring(0, 500)
        .trim()}...`;
      //location.href = '#productInfoHeading';
    } else {
      info = product.details.productInformation;
    }
  }

  return (
    <div className={styles.content}>
      <h2 id="productInfoHeading" className={styles.heading}>
        Product information
      </h2>

      <div className={styles.codePos1}>Product code: {product?.code}</div>

      <div
        className={styles.info}
        dangerouslySetInnerHTML={{ __html: info }}
        data-testid="product-info-text"
      />

      <div className={styles.codePos2}>Product code: {product?.code}</div>

      <div className={styles.more}>
        <div className={styles.moreTextSection}>
          <button className={styles.moreTextButton} onClick={onMoreClick}>
            {showFull ? 'Read less' : 'Read more'}
          </button>
        </div>
        <div className={styles.moreButtonSection}>
          <button className={styles.moreArrowButton} onClick={onMoreClick}>
            {showFull ? '<' : '>'}
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductInfoPanel;
