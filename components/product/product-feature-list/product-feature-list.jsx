import styles from './product-feature-list.module.scss';
import ProductFeatureListItem from '../product-feature-list-item/product-feature-list-item';

const ProductFeatureList = ({ product = {} }) => {

  const attributes = product?.details?.features[0]?.attributes;

  return (
    <div className={styles.content}>
      <h2>Product specification</h2>

      {attributes ? (
        <ul className={styles.list} aria-label="Product Specification List">
          {attributes.map((feature, index) => (
            <li key={index} className={styles.listItem}>
              <ProductFeatureListItem feature={feature} />
            </li>
          ))}
        </ul>
      ) : (
        <div aria-label="No Product Specification available">None</div>
      )}
    </div>
  );
};

export default ProductFeatureList;
