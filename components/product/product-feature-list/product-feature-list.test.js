import { render, screen } from '@testing-library/react';
import ProductFeatureList from './product-feature-list';
import mockData from '../../../mockData/data2.json';

const product = mockData.detailsData[0];

describe('Product Feature List Tests', () => {
  it('Renders a feature list', () => {
    render(<ProductFeatureList product={product} />);

    //const div = screen.queryByText(new RegExp('Boasting plenty of convenient features', 'i'));
    //expect(div).toBeInTheDocument();
  });
});

// unit test "unhappy" paths
// describe('Product Feature List Tests', () => {
//   it('Renders blank details text when incorrect object structure passed, i.e. does not crash!', () => {
//     const noData = {};
//     render(<ProductFeatureList product={noData} />);

//     const div = screen.getByTestId('product-info-text');
//     expect(div).toHaveTextContent('None');
//     //screen.debug();
//   });
// });

// describe('Product Info Panel Tests', () => {
//   it('Renders blank details text when no props passed, i.e. does not crash!', () => {
//     render(<ProductFeatureList />);

//     const div = screen.getByTestId('product-info-text');
//     expect(div).toBeEmptyDOMElement();
//     //screen.debug();
//   });
// });
