import { render, screen } from '@testing-library/react';
import ProductPricePanel from './product-price-panel';
import mockData from '../../../mockData/data2.json';

const product = mockData.detailsData[0];

describe('Product Price Panel Tests', () => {
  // quick sanity check
  it('Renders product price', () => {
    render(<ProductPricePanel product={product} />);

    const div = screen.queryByText(new RegExp(product.price.now, 'i'));
    expect(div).toBeInTheDocument();
  });
  
  // unit test "unhappy" paths
  it('Renders no price info when not provided in object - showing 0 price or crashing would not be a good idea!', () => {
    const noData = {};
    render(<ProductPricePanel product={noData} />);
    
    const div = screen.queryByTestId('product-price');
    expect(div).toBeNull();
    //screen.debug();
  });
  
  it('Renders no price info when props not provided - showing 0 price or crashing would not be a good idea!', () => {
    render(<ProductPricePanel />);
    
    const div = screen.queryByTestId('product-price');
    expect(div).toBeNull();
    //screen.debug();
  });
  
});