import styles from './product-price-panel.module.scss';

const ProductPricePanel = ({ product }) => {
  const price = `£${product?.price?.now}`;

  return (
    <div className={styles.content}>
      {product?.price?.now ? (
        <div
          className={styles.price}
          data-testid="product-price"
        >
          {price}
        </div>
      ) : null}
      <div
        className={styles.specialOffers}
        data-testid="product-offers"
      >
        {product?.displaySpecialOffer}
      </div>
      <div
        className={styles.includedServices}
        data-testid="product-services"
      >
        {product?.additionalServices?.includedServices}
      </div>
    </div>
  );
};

export default ProductPricePanel;
