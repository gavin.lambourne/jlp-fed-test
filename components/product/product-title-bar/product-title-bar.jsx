import Link from 'next/link';
import styles from './product-title-bar.module.scss';


const ProductTitleBar = ({ product }) => {
  return (
    <div className={styles.content}>
      <div className={styles.nav}>
        <Link
          href={{
            pathname: '/',
          }}
        >
          <a className={styles.link} aria-label="back">
            &lt;
          </a>
        </Link>
      </div>
      <div className={styles.title}>
        <h1>
          <div dangerouslySetInnerHTML={{ __html: product?.title }} />
        </h1>
      </div>
    </div>
  );
};

export default ProductTitleBar;
