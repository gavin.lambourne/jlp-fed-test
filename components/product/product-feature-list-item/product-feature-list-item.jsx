import styles from './product-feature-list-item.module.scss';
import Link from 'next/link';

const ProductFeatureListItem = ({ feature }) => {

  return feature ? (
    <div className={styles.content}>
      <div
        className={styles.title}
        dangerouslySetInnerHTML={{ __html: feature.name }}
        data-testid="feature-name"
      />
      <div
        className={styles.value}
        dangerouslySetInnerHTML={{ __html: feature.value }}
        data-testid="feature-value"
      />
    </div>
  ) : null;
};

export default ProductFeatureListItem;
