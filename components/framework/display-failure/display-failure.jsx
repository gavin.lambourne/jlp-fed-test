import styles from './display-failure.module.scss';

const DisplayFailure = ({ failureMessage = 'Sorry! something went wrong' }) => {
  return (
    <div>
      <div className={styles.content}>
        {failureMessage}
      </div>
    </div>
  );
};

export default DisplayFailure;
