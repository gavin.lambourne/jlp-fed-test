import { dishwashersUrl, dishwasherUrl } from '../config';
import { API_KEY } from '../config';
import { logger } from '../utils/logging'

import fetcher from './fetcher';

export const getAllDishwashers = async (page = 0, pageSize = 0) => {
  logger('getAllDishwashers: starting');

  let finalUrl = `${dishwashersUrl}&key=${API_KEY}`;
  finalUrl += page > 0 ? `&page=${page}` : '';
  finalUrl += pageSize > 0 ? `&pageSize=${pageSize}` : '';
  // improve this query string building.... use new URL()
  logger(`getAllDishwashers: finalUrl=${finalUrl}`);

  logger('getAllDishwashers: Getting data');
  const results = await fetcher(finalUrl);
  logger('getAllDishwashers: finished');
  return results;
};


export const getDishwasher = async (id) => {
  logger('getDishwasher: starting');

  let finalUrl = `${dishwasherUrl}${id}`;
  logger(`getDishwasher: finalUrl=${finalUrl}`);

  logger('getDishwasher: Getting data');
  const results = await fetcher(finalUrl);
  logger('getAllDishwashers: finished');
  return results;
};
