import { logger } from '../utils/logging';

const fetcher = async (url) => {
  logger(`fetcher: middleware started`);

  const headers = {
    'Content-Type': 'application/json',
    'User-Agent': 'dishwasher-app',
  };

  logger(`fetcher: using url: ${url}`);
  const res = await fetch(url, {
    method: 'GET',
    headers: headers,
  });

  logger(`fetcher: res.ok=${res.ok}`);
  logger(`fetcher: res.status is: ${res.status}`);
 
  if (!res.ok) {
    const error = new Error('An error occurred while fetching the data.');
    error.info = await res.json();
    error.status = res.status;
    throw error;
  }

  return res.json();
};

export default fetcher;
