import { render, screen } from '@testing-library/react';
import Home from '../pages/index';
import { getServerSideProps } from '../pages/index';
import mockData from '../mockData/data.json';

//const productCount = mockData.products.length;
//const products = mockData.products;

const productCount = 3;
const products = mockData.products.slice(0, productCount);
mockData.products = products;

global.fetch = jest.fn(() => {
  //console.log('Mock fetch() Running instead of real fetch() !');
  return Promise.resolve({
    json: () => Promise.resolve(mockData),
    ok: true,
    status: '200',
  });
});

beforeEach(() => {
  fetch.mockClear();
});

const renderPage = async () => {
  // mock result object
  const res = {
    setHeader: (header) => {},
  };

  // mock request object
  const req = {};

  const sspData = await getServerSideProps({ req, res });
  render(<Home data={sspData.props.data} ok={sspData.props.ok} />);
  return sspData;
};

describe('Index (Home) Tests', () => {

  // todo: add renders heading


  it('Renders expected number of products on page (no getServeSideProps)', () => {
    render(<Home data={mockData} ok={true} />);

    const elements = screen.getAllByTestId('product-item-title');
    expect(elements.length).toBe(productCount);
  });

  it('Renders expected Sorry! message when ok=false', () => {
    render(<Home data={{}} ok={false} />);

    const elements = screen.queryAllByText(/Sorry! Something went wrong/i);
    expect(elements.length).toBe(1);
  });

  it('Renders expected number of products on page', async () => {
    await renderPage();

    const elements = screen.getAllByTestId('product-item-title');
    expect(elements.length).toBe(productCount);
    expect(fetch).toBeCalledTimes(1);
  });

  it('Renders Sorry! message when GetServerSideProps Fails', async () => {
    fetch.mockImplementationOnce(() => Promise.reject('API Failed'));
    await renderPage();

    const elements = screen.queryAllByText(/Sorry! Something went wrong/i);
    expect(elements.length).toBe(1);
    expect(fetch).toBeCalledTimes(1);

  });

  it('Renders Sorry! message when server responds with 500', async () => {
    fetch.mockImplementationOnce(() => {
      return Promise.resolve({
        json: () => Promise.resolve({ error: 'Server having a bad day!' }),
        ok: false,
        status: '500',
      });
    });

    await renderPage();

    const elements = screen.queryAllByText(/Sorry! Something went wrong/i);
    expect(elements.length).toBe(1);
    expect(fetch).toBeCalledTimes(1);
  });

  it('Renders expected number of products on page', async () => {
    await renderPage();

    const elements = screen.queryAllByTestId('product-item-title');
    expect(elements.length).toBe(productCount);
  });

  it('Renders each product with correct product title', async () => {
    await renderPage();

    const elements = screen.queryAllByTestId('product-item-title');
    products.forEach((product, index) => {
      expect(elements[index]).toHaveTextContent(product.title);
    });
  });

  it('Renders each product with correct image details (alt/src)', async () => {
    await renderPage();

    const elements = screen.queryAllByRole('img');
    expect(elements.length).toBe(mockData.products.length);

    products.forEach((product, index) => {
      expect(elements[index]).toHaveAttribute('src', product.image);
    });

    products.forEach((product, index) => {
      expect(elements[index]).toHaveAttribute('alt', product.title);
    });
  });

  it('Renders each product with correct PRICE', async () => {
    await renderPage();

    const elements = screen.getAllByTestId('product-item-price');
    expect(elements.length).toBe(mockData.products.length);
    products.forEach((product, index) => {
      expect(elements[index]).toHaveTextContent(product.price.now);
    });
  });

  it('Renders each product with correct LINK/HREF', async () => {
    await renderPage();

    const elements = screen.getAllByTestId('product-item-link');
    expect(elements.length).toBe(productCount);
    products.forEach((product, index) => {
      expect(elements[index]).toHaveAttribute(
        'href',
        `/product-detail/${product.productId}`
      );
    });
  });
});
