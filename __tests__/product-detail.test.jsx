import { fireEvent, render, screen } from '@testing-library/react';
import ProductDetail from '../pages/product-detail/[id]';
import { getServerSideProps } from '../pages/product-detail/[id]';

import mockData from '../mockData/data2.json';

// TESTS FIRST!

// PAGE / DATA FETCHING
//  Test ServerSideProps => mock fetch / axios?
//  Query parameter validation/injection?

// HEADING BAR
//  Renders heading - done
//  Renders back button with correct link - done
//  Back button a11y support - find by role/aria-label? - done

// IMAGE SLIDER
//  Renders all images - done
//  Only one image visible
//  Renders Back/Next buttons - find by tole a11y? - done
//  Renders 1 dot button for each image - done
//

// IMAGE SLIDER interaction
//  Back button shows previous image
//  Next button shows next image
//  Back on first image goes to last image
//  Next on last image goes to first image
//  Correct dot highlighted for selected image
//  Clicking on dot selects correct image - click on all (or maybe first/last dot)

// LAYOUT
//  Layout changes Price section positioning for landscape

// PRODUCT INFORMATION
//  Section heading rendered - done
//  More/Less a11y - find by role / aria-label / ensure a button (semantic) - done
//  Renders "more" on inital render - done
//  Should only rended more/less in landscape layout? KISS always rended as less - nfa
//  Renders less when "less" clicked
//  Renders more when "more" clicked

// FEATURES LIST
//  Renders all features on product - count - done
//  Renders expected description + value for each feature - done
//  A11y - Rendered as a list - semantic - done?

const product = mockData.detailsData[0];

global.fetch = jest.fn(() => {
  //console.log('Mock fetch() Running instead of real fetch() !');
  return Promise.resolve({
    json: () => Promise.resolve(product),
    ok: true,
    status: '200',
  });
});

beforeEach(() => {
  fetch.mockClear();
});

const renderPage = async () => {
  // mock result object
  const res = {
    setHeader: (header) => {},
  };

  // mock request object
  const params = { id: product.productId };

  const sspData = await getServerSideProps({ params, res });
  render(
    <ProductDetail product={sspData.props.product} ok={sspData.props.ok} />
  );

  return sspData;
};

const renderProductDetail = (product) => {
  render(<ProductDetail product={product} ok={true} />);
};

describe('Product Detail Page - Heading Bar Tests', () => {
  it('Renders Product heading', async () => {
    await renderPage();

    const heading = screen.getByText(product.title);
    expect(heading).toBeInTheDocument();
  });

  it('Renders "Back" link with a11y support to navigate to home page', async () => {
    await renderPage();

    const heading = screen.getByRole('link', { name: 'back' });
    expect(heading).toBeInTheDocument();
    expect(heading).toHaveAttribute('href', '/');
  });
});

describe('Product Detail Page - Product Information Tests', () => {
  it('Renders Product Information heading', async () => {
    await renderPage();

    const heading = screen.getByText(/Product Information/i);
    expect(heading).toBeInTheDocument();
  });

  it('Renders product info from product (warning: only checks first 40 chars exist)', async () => {
    await renderPage();

    const div = screen.queryByText(
      new RegExp('Boasting plenty of convenient features', 'i')
    );
    expect(div).toBeInTheDocument();
  });

  it('Renders More/Less button as a More button on first render', async () => {
    await renderPage();

    const button = screen.queryByRole('button', { name: 'Read more' });
    expect(button).toBeInTheDocument();
  });

  it('Renders Full product information when "Read More" is clicked and reduces product info text when clicked again', async () => {
    await renderPage();

    const button = screen.queryByRole('button', { name: 'Read more' });
    expect(button).toBeInTheDocument();
    fireEvent.click(button);

    const div = screen.queryByText(new RegExp('Quick 45 °C wash', 'i'));
    expect(div).toBeInTheDocument();

    fireEvent.click(button);

    const div2 = screen.queryByText(new RegExp('Quick 45 °C wash', 'i'));
    expect(div2).not.toBeInTheDocument();
  });
});

describe('Product Detail Page - Features Tests', () => {
  it('Renders Product Specification heading', async () => {
    await renderPage();

    const heading = screen.queryByText(/Product Specification/i);
    expect(heading).toBeInTheDocument();
  });

  it('Renders correct number of product features with correct name/value pairs', async () => {
    await renderPage();

    const expectedLength = product.details.features[0].attributes.length;

    const nameDivs = screen.queryAllByTestId('feature-name');
    expect(nameDivs).toHaveLength(expectedLength);

    const valueDivs = screen.queryAllByTestId('feature-value');
    expect(valueDivs).toHaveLength(expectedLength);

    product.details.features[0].attributes.forEach((feature, index) => {
      expect(nameDivs[index]).toHaveTextContent(feature.name);
      expect(valueDivs[index]).toHaveTextContent(feature.value);
    });
  });
});

describe('Product Detail Page - Price Panel', () => {
  it('Renders Product Price', async () => {
    await renderPage();

    const prices = screen.queryAllByText(new RegExp(product.price.now, 'i'));
    expect(prices).toHaveLength(1);
    expect(prices[0]).toBeInTheDocument();
  });

  it('Renders Product Special Offers', async () => {
    await renderPage();

    const offers = screen.queryAllByText(new RegExp(product.displaySpecialOffer, 'i'));
    expect(offers).toHaveLength(1);
    expect(offers[0]).toBeInTheDocument();
  });

  it('Renders Product Included Services', async () => {
    await renderPage();

    const services = screen.queryAllByText(
      new RegExp(product.additionalServices.includedServices, 'i')
    );

    expect(services).toHaveLength(1);
    expect(services[0]).toBeInTheDocument();
  });
});

describe('Product Detail Page - Carousel Tests', () => {
  it('Renders Previous Button', async () => {
    await renderPage();

    const buttons = screen.queryAllByRole('button', {
      name: 'Previous Product Image',
    });

    expect(buttons).toHaveLength(1);
    expect(buttons[0]).toBeInTheDocument();
  });

  it('Renders Next Button', async () => {
    await renderPage();

    const buttons = screen.queryAllByRole('button', {
      name: 'Next Product Image',
    });

    expect(buttons).toHaveLength(1);
    expect(buttons[0]).toBeInTheDocument();
  });

  const expectedDotCount = product.media.images.urls.length;
  it('Renders All Dot Buttons - 1 per image', async () => {
    await renderPage();

    const buttons = screen.queryAllByRole('button', {
      name: /Select Product Image/i,
    });

    expect(buttons).toHaveLength(expectedDotCount);
    for (let btnIndex = 0; btnIndex < expectedDotCount; btnIndex++) {
      expect(buttons[btnIndex]).toBeInTheDocument();
    }
  });

  it('Renders All Carousel Images with correct alt and src ', async () => {
    await renderPage();

    const buttons = screen.queryAllByRole('img', {
      name: product.title, // only mock data has: product.media.images.altText,
    });

    expect(buttons).toHaveLength(expectedDotCount);
    for (let btnIndex = 0; btnIndex < expectedDotCount; btnIndex++) {
      expect(buttons[btnIndex]).toBeInTheDocument();
      expect(buttons[btnIndex]).toHaveAttribute(
        'src',
        `${product.media.images.urls[btnIndex]}$rsp-pdp-port-640$`
      );
    }
  });
});
